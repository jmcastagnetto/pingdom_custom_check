<?php
// (c) 2014, Jesus M. Castagnetto
// License: BSD 2-Clause (http://opensource.org/licenses/BSD-2-Clause)
$start = microtime(true);

// Thresholds

define('SWAP_THRESHOLD', 512); // 512 MB of swap usage
define('HD_THRESHOLD', 90); // 90% of space used
define('HD_MOUNT', '\/dev\/sda1'); // filesystem mount to check
define('LOADAVG5_THRESHOLD', 2.0); // the 5 minute CPU load average
define('DB_DSN', 'mysql:host=localhost;dbname=testdb');
define('DB_USER', 'user');
define('DB_PASSWORD', 'password');
define('DB_CPU_THRESHOLD', 90); // 90% CPU usage by database

// checks each item against a threshold
function item_check($command, $threshold, $check) {
    $item = `$command`;
    if ($item > $threshold) {
        return $check." OVER THRESHOLD";
    } else {
        return "";
    }
}

// creates the final status string
function build_status($msgarr) {
    $status = "";
    foreach ($msgarr as $msg) {
        if ("" !== $msg) {
            $status .= ",$msg";
        }
    }
    if ("" === $status) {
        return "OK";
    } else {
        return substr($status, 1);
    }
}

// check swap usage
$swap = item_check("free -m | awk '/^Swap:/{ print $3 }'",
                    SWAP_THRESHOLD, "SWAP USAGE");

// check load average
$loadavg = item_check("uptime | awk -F ', ' '{ print $5 }'",
                       LOADAVG5_THRESHOLD, "LOAD AVERAGE");

// check disk usage
$disk = item_check("df -h | awk '/".HD_MOUNT."/{print substr($5, 0, length($5)-1)}'",
                    HD_THRESHOLD, "DISK USAGE");

// check connectivity
try {
    $dbc = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
} catch (PDOException $e) {
    $dbconnect = "ERROR CONNECTING TO DATABASE";
}

// check MySQL cpu usage
$dbcpu = item_check("top -b -n 1 | awk '/mysql/ {print $9}' | sort -nr | head -1",
                     DB_CPU_THRESHOLD, "CPU USAGE BY DATABASE");

$status = build_status(array($swap, $loadavg, $disk, $dbconnect, $dbcpu));
$responsetime = round(1000*(microtime(true) - $start), 3);

$xml = <<<EOS
<?xml version="1.0" encoding="UTF-8"?>
<pingdom_http_custom_check>
<status>$status</status>
<response_time>$responsetime</response_time>
</pingdom_http_custom_check>
EOS;

header('Content-Type: application/xml');
echo $xml;
